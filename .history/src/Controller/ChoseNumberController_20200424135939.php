<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ChoseNumberController extends AbstractController
{
    /**
     * @Route("/chose/number", name="chose_number")
     */
    public function index()
    {
        $num1 =  random_int(0, 100);

        return $this->render('chose_number/index.html.twig', [
            'controller_name' => 'ChoseNumberController',
            'num_1' => $num1
        ]);
    }
}
