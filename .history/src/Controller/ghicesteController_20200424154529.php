<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GhicesteController extends AbstractController
{
    /**
     * @Route("/ghiceste/numar", name="ghiceste_numar")
     */
    public function index()
    {
        $num1 =  random_int(0, 100);

        return $this->render('ghiceste_numar/index.html.twig', [
            'controller_name' => 'GhicesteController',
            'num_1' => $num1
        ]);
    }
}
